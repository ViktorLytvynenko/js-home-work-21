document.body.style.textAlign = "center"
let firstButtonElement = document.getElementById("firstButton");
let secondButton;
firstButtonElement.addEventListener("click", function () {
    if (document.querySelector("input") !== null) {
    } else {
        let inputElement = document.createElement("input");
        document.body.append(inputElement);
        inputElement.setAttribute("type", "number");
        inputElement.setAttribute("placeholder", "Діаметр кола");
        secondButton = document.createElement("button");
        document.body.append(secondButton);
        secondButton.innerText = "Намалювати";
        secondButton.addEventListener("click", function () {
            let container = document.createElement("div");
            document.body.append(container);
            container.style.width = "100%";
            container.style.display = "flex";
            container.style.flexWrap = "wrap";
            container.style.marginTop = "50px";
            let diameter = inputElement.value;
            for (let i = 0; i < 10; i++) {
                let rowElement = document.createElement("div");
                rowElement.classList.add("circle-row");
                rowElement.style.display = "flex";
                rowElement.style.flexDirection = "row";
                rowElement.style.margin = "0 auto";
                for (let j = 0; j < 10; j++) {
                    let circleElement = document.createElement("div");
                    circleElement.classList.add("circle");
                    circleElement.style.width = `${diameter}px`;
                    circleElement.style.height = `${diameter}px`;
                    circleElement.style.borderRadius = "50%";
                    let red = Math.floor(Math.random() * 256);
                    let green = Math.floor(Math.random() * 256);
                    let blue = Math.floor(Math.random() * 256);
                    let randomColor = `rgb(${red}, ${green}, ${blue})`;
                    circleElement.style.backgroundColor = randomColor;
                    rowElement.prepend(circleElement);
                    circleElement.addEventListener("click", function () {
                        circleElement.remove();
                    });
                }
                container.append(rowElement);
            }
        });
    }
});